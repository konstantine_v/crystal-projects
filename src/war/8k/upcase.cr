def is_uppercase(str)
  str.upcase == str
end

# Tests
puts is_uppercase("Hello There")
puts is_uppercase("AHHHHHHHHH")

# Tests Pass, Works

# Codewars Basic tests don't pass but Full-Suite does
