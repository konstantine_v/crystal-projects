# first attempt
def better_than_average(class_points : Array(Int32), your_points : Int32)
  your_points > class_points.sum/class_points.size ? true : false
end

# Refactor
def better_than_average(class_points : Array(Int32), your_points : Int32)
  your_points > class_points.sum/class_points.size
end
# Another case where having t/n return values are redundant

# Tests
puts better_than_average([100, 40, 34, 57, 29, 72, 57, 88], 75)

# Works, Tested
