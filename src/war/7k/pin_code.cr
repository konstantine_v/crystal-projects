def validate_pin(pin)
  puts pin
  case
  when pin.includes?("a") then false
  when pin.count("0-9", "a-z", "A-Z") > 4 then false
  when pin.count("0-9", "a-z", "A-Z") < 4 then false
  # when pin count < 4 then false
  else true
  end
end

puts validate_pin("-1884")
puts validate_pin("1884")
puts validate_pin("1asd84")
puts validate_pin("ddasdasggwge")
