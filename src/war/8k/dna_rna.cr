# First Attempt
def dna_to_rna_old(dna)
  dna.gsub("T"){"U"}
end

# Refactor
def dna_to_rna(dna)
  dna.gsub('T', 'U')
end
# Notice the use of chars rather than strings as well as them being in the same function

# Test
puts dna_to_rna("GCAT")
puts dna_to_rna("GCGGGGAAAGGGGGGGGCCCCCCACCCCCAAT")
puts dna_to_rna("GTCCATTGTGAA")
puts dna_to_rna("GATATAGGACCAGCCCCC")

# Passes Works
