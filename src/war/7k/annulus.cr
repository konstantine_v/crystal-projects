def annulus_area(r, R)
  # Area of annulus
  # πR2 − πr2
  a=3.14((R**2)(r**2))
end

# Tests
puts annulus_area(7)
puts annulus_area(13)

# Couldn't test due to R not being included in the project so the Kata didn't work.
# this does basically what it wants by correctly calculating the annulus area
