# First Pass
def bmi(weight, height)
  bmi = weight/(height*height)
  if bmi <= 18.5
    return "Underweight"
  elsif bmi <= 25.0
    return "Normal"
  elsif bmi <= 30.0
    return "Overweight"
  elsif bmi > 30
    return "Obese"
  else nil
  end
end

# Simple BMI Script
def bmi(weight, height)
  b = weight / height ** 2
  case
    when b <= 18.5 then "Underweight"
    when b <= 25.0 then "Normal"
    when b <= 30.0 then "Overweight"
    else "Obese"
  end
end
