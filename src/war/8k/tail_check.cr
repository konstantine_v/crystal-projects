# First Pass
def correct_tail(body, tail)
  body[-1].to_s == tail
end

# Refactor
def correct_tail(a, b)
  a.ends_with?(b)
end
# Same as above but cleaner.
# You can check the ending of any string with x.ends_with?(y)

# Tests
puts correct_tail("Fox", "x")
puts correct_tail("Fox", "t")

# Tests Pass
