# First Pass
def quote(fighter)
  fighters_arr = ["George Saint Pierre", "Conor McGregor"]
  quotes_arr = ["I am not impressed by your performance.", "I'd like to take this chance to apologize.. To absolutely NOBODY!"]
  case fighter
  when fighters_arr[0] then quotes_arr[0]
  when fighters_arr[0].upcase then quotes_arr[0]
  when fighters_arr[0].downcase  then quotes_arr[0]
  when fighters_arr[0].capitalize then quotes_arr[0]
  when fighters_arr[1] then quotes_arr[1]
  when fighters_arr[1].upcase then quotes_arr[1]
  when fighters_arr[1].downcase then quotes_arr[1]
  when fighters_arr[1].capitalize then quotes_arr[1]
  else nil
  end
end

# Refactor
def quote(fighter)
  fighters_arr = ["George Saint Pierre", "Conor McGregor"]
  quotes_arr = ["I am not impressed by your performance.", "I'd like to take this chance to apologize.. To absolutely NOBODY!"]
  fighter.downcase === fighters_arr[0].downcase ? quotes_arr[0] : quotes_arr[1]
end
# This is what sanitizing input looks like, I should do better to remember this.

# The confusion here is that I was assuming the data couldn't be changed, thinking it to be like a database situation. The refactored version still takes that into account.
