require "email"
require "./env" # ENV for variables

config = EMail::Client::Config.new(
  ENV["EMAIL_HOST"],
  ENV["EMAIL_POST"].to_i,
  helo_domain: ENV["EMAIL_HOST"])
config.use_tls(EMail::Client::TLSMode::STARTTLS)
config.tls_context.add_options(OpenSSL::SSL::Options::NO_SSL_V2 | OpenSSL::SSL::Options::NO_SSL_V3 | OpenSSL::SSL::Options::NO_TLS_V1 | OpenSSL::SSL::Options::NO_TLS_V1_1)
config.tls_context.verify_mode = OpenSSL::SSL::VerifyMode::NONE
config.use_auth(
  ENV["EMAIL_U"],
  ENV["EMAIL_P"])
# client = EMail::Client.new(config)

EMail.send(config) do
  from    ENV["EMAIL_F"]
  to      ENV["EMAIL_T"]
  subject "Test Email"
  message <<-EOM
  This is a test email
  Some Text Here...
  --
  Insert Signature Here...
  EOM
end
